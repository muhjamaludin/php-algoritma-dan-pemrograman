<?php

// menyimpan file yang di upload

if ($_FILES["file"]["type"] == "image/gif" || 
($_FILES["file"]["type"] == "image/jpeg") &&
($_FILES["file"]["size"] < 100000)) {
    if ($_FILES['file']['error'] > 0) {
        echo "Return Code: ".$_FILES["file"]["error"]."<br>";
    } else {
        echo "Upload: ". $_FILES["file"]["name"]. "<br>";
        echo "Type: ". $_FILES["file"]["type"]. "<br>";
        echo "Size: ". ($_FILES["file"]["size"]/1024). " KB <br>";
        echo "From tmp : ". $_FILES["file"]["tmp_name"]."<br>";
        // print_r($_FILES["file"]);
        if (file_exists("upload/". $_FILES["file"]["name"])) {
            echo $_FILES["file"]["name"]. " already exists.";
        } else {
            move_uploaded_file($_FILES["file"]["tmp_name"],
            "upload/". $_FILES["file"]["name"]);
            echo "Stored in: ". "upload/". $_FILES["file"]["name"];
        }
    }
} else {
    echo "File more than 100KB or not gif/jpeg";
}