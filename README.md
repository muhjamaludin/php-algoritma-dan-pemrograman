# Tutorial Algoritma dan Pemrograman #

### Prerequisite
- php 7.4.3

### How to run
- using built in web server `php -S localhost:<port>`
- open browser localhost:<port>/<file>

This tutorial inspired by book 

![Algoritma dan Pemrograman](https://bitbucket.org/muhjamaludin/php-algoritma-dan-pemrograman/raw/8bd3b9e6191a5a602dc6fab69e2bf819eac48690/assets/images/Screenshot_2021-03-23%20Algoritma%20Pemrograman.png)

